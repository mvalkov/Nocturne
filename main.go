package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sync"
	"time"
)

var (
	aacRegxp, _        = regexp.Compile(`.aac$`)
	incompleteRegxp, _ = regexp.Compile("incomplete")
	megabyte           = int64(1048576)
	minFileSize        = 2 * megabyte
	maxFileSize        = 50 * megabyte
)

func main() {
	// Парсим флаги запуска
	inputFile := flag.String(
		"in", "", "-in=/path/to/*.m3u_file",
	)
	outDir := flag.String(
		"out", "", "-out=/path/to/dir/for/save/music",
	)
	flag.Parse()

	// Проверка на наличие указанного пути и является ли дирректорией
	fileInfo, statErr := os.Stat(*outDir)
	if statErr != nil || fileInfo.IsDir() == false {
		log.Fatal("wrong out dir")
	}

	// Чистим директорию для сохранения музыки от старых incomplete
	err := GarbageCollector(*outDir)
	if err != nil {
		log.Println("something went wrong with cleaning up the incomplete folders")
	} else {
		log.Println("incomplete folders cleaned")
	}

	// Чистим папку от incomplete и дубликатов
	// если указан только флаг выходной директории
	if *inputFile == "" {
		log.Println("start cleaning dir")
		GarbageCollector(*outDir)
		log.Println("cleaned")
		os.Exit(0)
	}

	// Читаем файл с адресами радио станций и заполняем слайс
	file, fileErr := os.Open(*inputFile)
	if fileErr != nil {
		log.Fatal("path to m3u file not exist")
	}

	var radioAddress []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text()[:4] == "http" {
			radioAddress = append(radioAddress, scanner.Text())
		}
	}
	file.Close()
	log.Printf(
		"reading m3u file completed. [%d radio chanels found]",
		len(radioAddress),
	)

	// Ищем путь к бинарнику streamripper
	streamRipper, lookErr := exec.LookPath("streamripper")

	if lookErr != nil {
		log.Fatalln("streamripper not found.")
	}
	log.Println("streamripper found in", streamRipper)

	var wg sync.WaitGroup

	wg.Add(2)
	go Watcher(*outDir)
	go IncompleteCleaner(*outDir)

	// Запускаем стримрип горутины
	wg.Add(len(radioAddress))
	for id, address := range radioAddress {
		args := []string{
			address,
			"-o",
			"larger",
			"-d",
			*outDir,
			"--quiet",
		}
		go RunStreamRipper(id, streamRipper, args)
	}
	wg.Wait()
}

// запускает в потоках стримрипер
func RunStreamRipper(id int, streamRipper string, args []string) {
	for {
		cmd := exec.Command(streamRipper, args...)
		log.Println(id, args[0], "starting..")
		cmd.Run()
		log.Println(id, args[0], "crashed. Wait 5 min and try restart it")
		time.Sleep(time.Minute * 5)
	}
}

// чистит incomplete при запуске
func GarbageCollector(outDir string) error {
	errWalk := filepath.Walk(outDir,
		// лямбда для обработки выданого Walk пути
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				log.Print(err)
				return nil
			}
			// удаляет файлы меньше 2 МиБ
			isAac := aacRegxp.MatchString(info.Name())
			if isAac && info.Size() < 2097000 {
				os.Remove(path)
				return nil
			}
			// удаляет странные имена файлов
			if info.Name() == " - .aac" || info.Name() == " - -.aac" {
				os.Remove(path)
			}
			if info.IsDir() && info.Name() == "incomplete" {
				os.RemoveAll(path)
				return filepath.SkipDir
			}
			return nil
		})
	return errWalk
}

// Считает колво новых, модиф песен и общее колво
func Watcher(path string) {
	prevAllMusic := 0
	newTracks := 0
	currentTime := time.Now()
	log.Println("Watcher is started")
	for {
		modCount := 0
		allMusic := 0
		filepath.Walk(path,
			func(path string, info os.FileInfo, err error) error {
				// не искать в incomplete
				if info.IsDir() && info.Name() == "incomplete" {
					return filepath.SkipDir
				}
				// удаляет файлы меньше 2 МиБ
				isAac := aacRegxp.MatchString(info.Name())
				if isAac && info.Size() < minFileSize {
					os.Remove(path)
					return nil
				}
				//удаляет файлы больше 50 миб
				if isAac && info.Size() > maxFileSize {
					os.Remove(path)
					return nil
				}
				// удаляет странные имена файлов
				if info.Name() == " - .aac" || info.Name() == " - -.aac" {
					os.Remove(path)
				}
				if isAac {
					allMusic++
					// подсчет общего колва модиф треков
					if currentTime.Before(info.ModTime()) {
						modCount++
					}
				}
				return nil
			})
		if prevAllMusic != 0 {
			newTracks = allMusic - prevAllMusic
		}
		prevAllMusic = allMusic
		log.Printf("[ +%d mod ] [ +%d new ] [ %d all ]", modCount, newTracks, allMusic)
		currentTime = time.Now()
		time.Sleep(time.Hour)
	}
}

// чистит incomplete в процессе работы
func IncompleteCleaner(path string) {
	currentTime := time.Now().Add(-time.Hour)
	log.Println("Inclomplete cleaner is started.")
	for {
		counter := 0
		filepath.Walk(path,
			func(path string, info os.FileInfo, err error) error {
				if !incompleteRegxp.MatchString(path) {
					return nil
				}
				if !aacRegxp.MatchString(info.Name()) {
					return nil
				}
				if info.ModTime().After(currentTime) {
					return nil
				}
				os.Remove(path)
				counter++
				return nil
			})
		log.Printf("[ -%d incomplete files ]", counter)
		currentTime = time.Now()
		time.Sleep(time.Hour)
	}
}
